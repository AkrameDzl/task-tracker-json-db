import { useState } from "react"
const EditTask = ({ task, onEdit, showEditTask, setShowEditTask }) => {
  const [id] = useState(task.id)
  const [text, setText] = useState(task.text)
  const [day, setDay] = useState(task.day)
  const onSubmit = (e) => {
    e.preventDefault()
    if (!text) {
      alert("Please add a task")
      return
    }
    onEdit({ id, text, day })
    setText("")
    setDay("")
    setShowEditTask(!showEditTask)
  }
  return (
    <form className="add-form" onSubmit={onSubmit}>
      <div className="form-control">
        <label>Task</label>
        <input
          type="text"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
      </div>
      <div className="form-control">
        <label>Day & Time</label>
        <input
          type="text"
          value={day}
          onChange={(e) => setDay(e.target.value)}
        />
      </div>
      <input type="submit" value="Edit Task" className="btn btn-block" />
    </form>
  )
}

export default EditTask
