import { useState } from "react"
import { FaTimes, FaEdit, FaToggleOn, FaToggleOff } from "react-icons/fa"
import EditTask from "./EditTask"
const Task = ({ task, onDelete, onToggle, onEdit }) => {
  const [showEditTask, setShowEditTask] = useState(false)

  return (
    <div className={`task ${task.reminder ? "reminder" : ""}`}>
      <h3 style={{ display: "inline" }}>{task.text}</h3>
      <FaTimes
        size={21}
        className="icons"
        style={{
          color: "red",
          cursor: "pointer",
        }}
        onClick={() => onDelete(task.id)}
      />
      <FaEdit
        size={21}
        className="icons"
        style={{
          color: "green",
          cursor: "pointer",
        }}
        onClick={() => setShowEditTask(!showEditTask)}
      />
      <p>{task.day}</p>
      {task.reminder ? (
        <FaToggleOn
          size={25}
          className="icons"
          style={{
            position: "relative",
            top: "-20px",
            color: "green",
            cursor: "pointer",
          }}
          onClick={() => onToggle(task.id)}
        />
      ) : (
        <FaToggleOff
          size={25}
          className="icons"
          style={{
            position: "relative",
            top: "-20px",
            color: "red",
            cursor: "pointer",
          }}
          onClick={() => onToggle(task.id)}
        />
      )}
      {showEditTask && (
        <EditTask
          task={task}
          onEdit={onEdit}
          showEditTask={showEditTask}
          setShowEditTask={setShowEditTask}
        />
      )}
    </div>
  )
}

export default Task
